package cn.itcloud.stockseata.mapper;

import cn.itcloud.stockseata.entity.Stock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 17:23
 */
public interface StockMapper extends BaseMapper<Stock> {
}
