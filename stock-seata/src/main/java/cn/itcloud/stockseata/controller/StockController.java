package cn.itcloud.stockseata.controller;

import cn.itcloud.stockseata.entity.Stock;
import cn.itcloud.stockseata.servcie.StockService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 22:03
 */
@RestController
@RequestMapping("/api/stock")
public class StockController {

    @Autowired
    private StockService stockService;

    @GetMapping("/reduceStock")
    public String reduceStock(@RequestParam Integer productId, @RequestParam Integer totalAmount){
        LambdaQueryWrapper<Stock> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(Stock::getProductId,productId);
        Stock result = stockService.getOne(queryWrapper);
        result.setCount(result.getCount()-totalAmount);
        stockService.updateById(result);
        return "扣减库存成功";
    }

}
