package cn.itcloud.stockseata;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 17:21
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("cn.itcloud.stockseata.mapper")
public class StockSeataApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockSeataApplication.class,args);
    }


}
