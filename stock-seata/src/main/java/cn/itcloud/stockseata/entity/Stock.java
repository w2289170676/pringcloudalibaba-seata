package cn.itcloud.stockseata.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 17:24
 */
@Data
public class Stock {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer productId;
    private Integer count;

}
