package cn.itcloud.stockseata.servcie;

import cn.itcloud.stockseata.entity.Stock;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 17:25
 */
public interface StockService extends IService<Stock> {
}
