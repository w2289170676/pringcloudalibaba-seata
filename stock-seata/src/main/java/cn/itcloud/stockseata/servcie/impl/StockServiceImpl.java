package cn.itcloud.stockseata.servcie.impl;

import cn.itcloud.stockseata.entity.Stock;
import cn.itcloud.stockseata.mapper.StockMapper;
import cn.itcloud.stockseata.servcie.StockService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 17:26
 */
@Service
public class StockServiceImpl extends ServiceImpl<StockMapper, Stock> implements StockService {
}
