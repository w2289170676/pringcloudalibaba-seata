package cn.itcloud.common.core.exception;

import cn.itcloud.common.core.base.BaseException;

/**
 * @author zehua
 */
public class BizException extends BaseException {

    public BizException(ErrorMessage errorMessage) {
        super(errorMessage);
    }
    

}
