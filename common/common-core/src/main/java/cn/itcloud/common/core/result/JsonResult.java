package cn.itcloud.common.core.result;

import cn.itcloud.common.core.exception.BaseErrorEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zehua
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JsonResult<T> {
    private T data;
    private String message;

    public static <T> JsonResult<T> failed(BaseErrorEnum gatewayError) {
        JsonResult<T> result = new JsonResult<>();
        result.setMessage(gatewayError.getMessage());
        return result;
    }
}
