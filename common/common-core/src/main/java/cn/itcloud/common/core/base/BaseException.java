package cn.itcloud.common.core.base;

import cn.itcloud.common.core.exception.ErrorMessage;

/**
 * @author zehua
 */
public class BaseException extends RuntimeException {

    private String message;
    private String code;

    public BaseException(ErrorMessage errorMessage) {
        super(errorMessage.getMessage());
        this.message = errorMessage.getMessage();
        this.code = errorMessage.getCode();
    }

    public BaseException(String message) {
        super(message);
        this.message = message;
    }

    public BaseException(String message, String code) {
        super(message);
        this.message = message;
        this.code = code;
    }

    public BaseException(String message, Throwable throwable) {
        super(message, throwable);
        this.message = message;
    }

    public BaseException(String message, String code, Throwable throwable) {
        super(message, throwable);
        this.message = message;
        this.code = code;
    }
}
