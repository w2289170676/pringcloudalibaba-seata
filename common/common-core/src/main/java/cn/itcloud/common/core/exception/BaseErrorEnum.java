package cn.itcloud.common.core.exception;

/**
 * @author zehua
 */

public enum BaseErrorEnum implements ErrorMessage {

    //系统错误
    SYSTEM_ERROR("系统错误", "50000", "user"),
    GATEWAY_ERROR("GATEWAY APPEND ERROR", "50000", "user");

    String message;
    String code;
    String system;

    BaseErrorEnum(String message, String code, String system) {
        this.message = message;
        this.code = code;
        this.system = system;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getCode() {
        return this.message;
    }
}
