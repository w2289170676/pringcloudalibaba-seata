package cn.itcloud.common.core.exception;

/**
 * @author zehua
 */
public interface ErrorMessage {
    /**
     * 获取错误消息
     *
     * @return 错误消息
     */
    String getMessage();

    /**
     * 获取错误码
     *
     * @return 码
     */
    String getCode();
}
