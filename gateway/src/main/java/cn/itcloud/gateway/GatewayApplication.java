package cn.itcloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 15:36
 */
@SpringBootApplication(scanBasePackages = {"cn.itcloud"}, exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

}
