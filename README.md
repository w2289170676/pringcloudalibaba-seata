# springcloudalibaba-seata

#### 介绍
SpringCloudAlibaba 2021.0.1.0 版本整合分布式事务Seata（seata版本为1.4.2）

## 踩坑记录
[参见教程seata1.4.2配置](https://blog.csdn.net/sermonlizhi/article/details/123055175)

###主要配置如下
```
#分布式事务
seata:
  application-id: ${spring.application.name}
  tx-service-group: service-order-group  #配置事务分组  service.vgroupMapping.default_tx_group=default
  data-source-proxy-mode: AT
  registry:
    #配置seata配置中心,告诉seata client 怎么去访问 seata server（TC）
    type: nacos
    nacos:
      server-addr: 127.0.0.1:8848  # seata server 所在的nacoas服务地址
      application: seata-server    # 默认名称：seata-server 没有修改可以不配置
      group: SEATA_GROUP # 默认分组：SEATA_GROUP 没有修改可以不配置
      username: nacos
      password: nacos
      cluster: default
  service:
    vgroup-mapping:
      service-order-group: default
  config:
    type: nacos
    nacos:
      server-addr: 127.0.0.1:8848
      username: nacos
      password: nacos
      group: SEATA_GROUP
      data-id: seataServer.properties
  enabled: true
```
>注意项：
>>tx-service-group: service-order-group
>>tx-service-group: service-stock-group
>>不同服务组不一样
>
>> data-id: seataServer.properties不能错
> 
>>seata源码中script/config-center下的config.txt，需修改mysql的连接信息，nacos中新建配置
![nacos添加配置](add.png)
>>除了config.txt中的还需添加两项配置如下（此两项配置为项目中yml配置的组名），需加入到nacos配置中。
>>>service.vgroupMapping.service-stock-group=default
>>
>>>service.vgroupMapping.service-order-group=default
>>
>>然后保存配置
###其他配置参见以上教程链接
重点关注的几个文件file.conf，registry.conf，config.txt，主要修改mysql连接以及mode为db



