package cn.itcloud.elastic;

import cn.easyes.starter.register.EsMapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 15:36
 */
@SpringBootApplication(scanBasePackages = {"cn.itcloud.elastic"}, exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
@EnableFeignClients
@EsMapperScan("cn.itcloud.elastic")
public class ElasticApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticApplication.class,args);
    }

}
