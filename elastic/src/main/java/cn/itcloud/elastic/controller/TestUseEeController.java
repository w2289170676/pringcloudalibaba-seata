package cn.itcloud.elastic.controller;

import cn.easyes.core.conditions.LambdaEsQueryWrapper;
import cn.itcloud.elastic.dto.DocumentMapper;
import cn.itcloud.elastic.entity.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zehua
 */
@RestController
//@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TestUseEeController {
    @Autowired
    private DocumentMapper documentMapper;

    @GetMapping("/insert")
    public Integer insert() {
        // 初始化-> 新增数据
        Document document = new Document();
        document.setId("100");
        document.setTitle("老汉");
        document.setContent("推*技术过硬");
        return documentMapper.insert(document);
    }

    @GetMapping("/search")
    public List<Document> search() {
        // 查询出所有标题为老汉的文档列表
        LambdaEsQueryWrapper<Document> wrapper = new LambdaEsQueryWrapper<>();
        wrapper.eq(Document::getTitle, "老汉");
        return documentMapper.selectList(wrapper);
    }
}
