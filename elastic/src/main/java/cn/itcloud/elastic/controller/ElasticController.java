package cn.itcloud.elastic.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zehua
 */
@RestController
public class ElasticController {

    @GetMapping("/test")
    public String test() {
        return "hello world";
    }
}
