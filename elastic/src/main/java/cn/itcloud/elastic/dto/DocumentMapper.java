package cn.itcloud.elastic.dto;

import cn.easyes.core.conditions.interfaces.BaseEsMapper;
import cn.itcloud.elastic.entity.Document;

/**
 * Mapper
 * <p>
 * Copyright © 2021 xpc1024 All Rights Reserved
 **/
public interface DocumentMapper extends BaseEsMapper<Document> {
}
