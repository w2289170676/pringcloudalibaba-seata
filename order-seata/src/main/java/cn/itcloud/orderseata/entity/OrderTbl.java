package cn.itcloud.orderseata.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 16:25
 */
@Data
public class OrderTbl {

    @TableId(type = IdType.AUTO)
    private String id;
    private Integer productId;
    private Integer totalAmount;
    private String status;

}
