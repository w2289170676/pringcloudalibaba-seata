package cn.itcloud.orderseata.service.impl;

import cn.itcloud.orderseata.entity.OrderTbl;
import cn.itcloud.orderseata.mapper.OrderMapper;
import cn.itcloud.orderseata.api.OpenfeignService;
import cn.itcloud.orderseata.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 16:30
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, OrderTbl> implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OpenfeignService openfeignService;

    @Override
    @GlobalTransactional
    public boolean save(OrderTbl order) {
        //下单
        int a = orderMapper.insert(order);
        //扣减库存
        openfeignService.reduceStock(order.getProductId(),order.getTotalAmount());
//        int f = 1/0;
        if(a == 1) {
            return true;
        }
        return false;
    }
}
