package cn.itcloud.orderseata.service;

import cn.itcloud.orderseata.entity.OrderTbl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 16:29
 */
public interface OrderService extends IService<OrderTbl> {

    boolean save(OrderTbl order);

}
