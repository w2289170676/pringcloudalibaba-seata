package cn.itcloud.orderseata;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 15:36
 */
@SpringBootApplication(scanBasePackages = {"cn.itcloud"})
@EnableDiscoveryClient
@MapperScan("cn.itcloud.orderseata.mapper")
@EnableFeignClients
public class OrderSeataApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderSeataApplication.class,args);
    }

}
