package cn.itcloud.orderseata.mapper;

import cn.itcloud.orderseata.entity.OrderTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 16:28
 */
public interface OrderMapper extends BaseMapper<OrderTbl> {
}
