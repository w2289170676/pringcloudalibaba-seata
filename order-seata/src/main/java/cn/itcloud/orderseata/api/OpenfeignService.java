package cn.itcloud.orderseata.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zqing
 * @description: TODO
 * @date: 2022/7/18 22:21
 */
@FeignClient(value = "stock-server")
public interface OpenfeignService {

    @GetMapping("/api/stock/reduceStock")
    public String reduceStock(@RequestParam("productId") Integer productId,@RequestParam("totalAmount") Integer totalAmount);

}
