package cn.itcloud.orderseata.controller;

import cn.itcloud.common.log.annotation.SysLog;
import cn.itcloud.orderseata.entity.OrderTbl;
import cn.itcloud.orderseata.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zqing
 * @description TODO
 * @date 2022/7/18 16:45
 */
@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    private OrderService orderService;
    

    @SysLog("下单")
    @GetMapping("/save")
    public String save(){
        OrderTbl order = new OrderTbl();
        order.setProductId(1);
        order.setTotalAmount(10);
        order.setStatus("已支付");
        boolean result = orderService.save(order);
        if (!result) {
            return "下单失败";
        }
        return "下单成功";
    }

}
